package inheritance;
//Jamin Huang 1938040
public class BookStore {
	public static void main(String args[]) {
		Book [] bookarr=new Book[5];
		bookarr [0]=new Book("THE RETURN","Nicholas Sparks");
		bookarr [1]=new ElectronicBook("BATTLE GROUND","Jim Butcher",100);
		bookarr [2]=new Book("THE EVENING AND THE MORNING","Ken Follett");
		bookarr [3]=new ElectronicBook("VIOLET BENT BACKWARDS OVER THE GRASS","Lana Del Rey",80);
		bookarr [4]=new ElectronicBook("THE BOOK OF TWO WAYS","Jodi Picoult",200);
		
		for (int i=0;i<bookarr.length;i++) {
			System.out.println(bookarr[i].toString());
		}
	}
}
