package inheritance;
//Jamin Huang 1938040
public class ElectronicBook extends Book {
	int numberBytes;
	
	public ElectronicBook(String title,String author,int numberBytes) {
		super(title,author);
		this.numberBytes=numberBytes;
	}
	
	public String toString() {
		return title+" by "+super.getAuthor()+". Number of Bytes: "+numberBytes;
	}
}
