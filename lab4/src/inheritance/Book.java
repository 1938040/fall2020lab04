package inheritance;
//Jamin Huang 1938040
public class Book {
	protected String title;
	private String author;
	
	public String getTitle() {
		return title;
	}
	public String getAuthor() {
		return author;
	}
	public String toString() {
		return title+" by "+author;
	}
	
	public Book(String title, String author) {
		this.author=author;
		this.title=title;
	}
}
