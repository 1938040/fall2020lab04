package geometry;
//Jamin Huang 1938040
public class LotsOfShapes {

	public static void main(String[] args) {
		Shape[] shapearr=new Shape[5];
		shapearr[0]=new Rectangle(20,30);
		shapearr[1]=new Rectangle(20,2);
		shapearr[2]=new Circle(2.3);
		shapearr[3]=new Circle(12.932);
		shapearr[4]=new Square(20);
		
		for(Shape i:shapearr) {
			System.out.println("Area:"+i.getArea()+" , Perimeter: "+i.getPerimeter());
		}
	}
}
