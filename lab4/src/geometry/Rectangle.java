package geometry;
//Jamin Huang 1938040
public class Rectangle implements Shape {
	private double width;
	private double length;
	
	public Rectangle(double width,double length) {
		this.width=width;
		this.length=length;
	}
	public double getArea() {
		return width*length;
	}
	public double getPerimeter() {
		return (width+length)*2;
	}
}
