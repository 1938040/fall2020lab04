package geometry;
//Jamin Huang 1938040
public interface Shape {
	double getArea();
	double getPerimeter();
}
